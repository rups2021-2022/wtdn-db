FROM alpine:latest

ENV TIMEZONE Europe/Paris
ENV MYSQL_ROOT_PASSWORD root
ENV MYSQL_DATABASE wtdn
ENV MYSQL_USER rups
ENV MYSQL_PASSWORD rups123
ENV MYSQL_USER_MONITORING monitoring
ENV MYSQL_PASSWORD_MONITORING monitoring

# Installing packages MariaDB
RUN apk add --no-cache mysql
RUN addgroup mysql mysql

# Work path
WORKDIR /scripts

# Copy of the MySQL startup script
COPY start.sh start.sh

# Creating the persistent volume
VOLUME [ "/var/lib/mysql" ]

EXPOSE 3306

ENTRYPOINT [ "./start.sh" ]
