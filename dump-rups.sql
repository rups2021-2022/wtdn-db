--
-- PostgreSQL database dump
--

-- Dumped from database version 12.8 (Ubuntu 12.8-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 13.3

-- Started on 2021-11-02 19:50:34

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 16387)
-- Name: rups; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA rups;


ALTER SCHEMA rups OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 212 (class 1259 OID 16434)
-- Name: panel; Type: TABLE; Schema: rups; Owner: postgres
--

CREATE TABLE rups.panel (
    panel_id integer NOT NULL,
    name character varying NOT NULL,
    status_id integer
);


ALTER TABLE rups.panel OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16432)
-- Name: panel_panel_id_seq; Type: SEQUENCE; Schema: rups; Owner: postgres
--

CREATE SEQUENCE rups.panel_panel_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rups.panel_panel_id_seq OWNER TO postgres;

--
-- TOC entry 3051 (class 0 OID 0)
-- Dependencies: 211
-- Name: panel_panel_id_seq; Type: SEQUENCE OWNED BY; Schema: rups; Owner: postgres
--

ALTER SEQUENCE rups.panel_panel_id_seq OWNED BY rups.panel.panel_id;


--
-- TOC entry 222 (class 1259 OID 16530)
-- Name: panel_task; Type: TABLE; Schema: rups; Owner: postgres
--

CREATE TABLE rups.panel_task (
    panel_task_id integer NOT NULL,
    panel_id integer NOT NULL,
    task_id integer NOT NULL
);


ALTER TABLE rups.panel_task OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 16528)
-- Name: panel_task_panel_task_id_seq; Type: SEQUENCE; Schema: rups; Owner: postgres
--

CREATE SEQUENCE rups.panel_task_panel_task_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rups.panel_task_panel_task_id_seq OWNER TO postgres;

--
-- TOC entry 3052 (class 0 OID 0)
-- Dependencies: 221
-- Name: panel_task_panel_task_id_seq; Type: SEQUENCE OWNED BY; Schema: rups; Owner: postgres
--

ALTER SEQUENCE rups.panel_task_panel_task_id_seq OWNED BY rups.panel_task.panel_task_id;


--
-- TOC entry 214 (class 1259 OID 16460)
-- Name: project; Type: TABLE; Schema: rups; Owner: postgres
--

CREATE TABLE rups.project (
    project_id integer NOT NULL,
    name character varying NOT NULL,
    start_date date,
    due_date date,
    status_id integer
);


ALTER TABLE rups.project OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16512)
-- Name: project_panel; Type: TABLE; Schema: rups; Owner: postgres
--

CREATE TABLE rups.project_panel (
    project_panel_id integer NOT NULL,
    project_id integer NOT NULL,
    panel_id integer NOT NULL
);


ALTER TABLE rups.project_panel OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16510)
-- Name: project_panel_project_panel_id_seq; Type: SEQUENCE; Schema: rups; Owner: postgres
--

CREATE SEQUENCE rups.project_panel_project_panel_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rups.project_panel_project_panel_id_seq OWNER TO postgres;

--
-- TOC entry 3053 (class 0 OID 0)
-- Dependencies: 219
-- Name: project_panel_project_panel_id_seq; Type: SEQUENCE OWNED BY; Schema: rups; Owner: postgres
--

ALTER SEQUENCE rups.project_panel_project_panel_id_seq OWNED BY rups.project_panel.project_panel_id;


--
-- TOC entry 213 (class 1259 OID 16458)
-- Name: project_project_id_seq; Type: SEQUENCE; Schema: rups; Owner: postgres
--

CREATE SEQUENCE rups.project_project_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rups.project_project_id_seq OWNER TO postgres;

--
-- TOC entry 3054 (class 0 OID 0)
-- Dependencies: 213
-- Name: project_project_id_seq; Type: SEQUENCE OWNED BY; Schema: rups; Owner: postgres
--

ALTER SEQUENCE rups.project_project_id_seq OWNED BY rups.project.project_id;


--
-- TOC entry 216 (class 1259 OID 16476)
-- Name: project_users; Type: TABLE; Schema: rups; Owner: postgres
--

CREATE TABLE rups.project_users (
    project_users_id integer NOT NULL,
    project_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE rups.project_users OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16474)
-- Name: project_users_project_users_id_seq; Type: SEQUENCE; Schema: rups; Owner: postgres
--

CREATE SEQUENCE rups.project_users_project_users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rups.project_users_project_users_id_seq OWNER TO postgres;

--
-- TOC entry 3055 (class 0 OID 0)
-- Dependencies: 215
-- Name: project_users_project_users_id_seq; Type: SEQUENCE OWNED BY; Schema: rups; Owner: postgres
--

ALTER SEQUENCE rups.project_users_project_users_id_seq OWNED BY rups.project_users.project_users_id;


--
-- TOC entry 206 (class 1259 OID 16401)
-- Name: status; Type: TABLE; Schema: rups; Owner: postgres
--

CREATE TABLE rups.status (
    status_id integer NOT NULL,
    value character varying NOT NULL
);


ALTER TABLE rups.status OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16399)
-- Name: status_status_id_seq; Type: SEQUENCE; Schema: rups; Owner: postgres
--

CREATE SEQUENCE rups.status_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rups.status_status_id_seq OWNER TO postgres;

--
-- TOC entry 3056 (class 0 OID 0)
-- Dependencies: 205
-- Name: status_status_id_seq; Type: SEQUENCE OWNED BY; Schema: rups; Owner: postgres
--

ALTER SEQUENCE rups.status_status_id_seq OWNED BY rups.status.status_id;


--
-- TOC entry 204 (class 1259 OID 16390)
-- Name: tag; Type: TABLE; Schema: rups; Owner: postgres
--

CREATE TABLE rups.tag (
    tag_id integer NOT NULL,
    value character varying NOT NULL
);


ALTER TABLE rups.tag OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16388)
-- Name: tag_tag_id_seq; Type: SEQUENCE; Schema: rups; Owner: postgres
--

CREATE SEQUENCE rups.tag_tag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rups.tag_tag_id_seq OWNER TO postgres;

--
-- TOC entry 3057 (class 0 OID 0)
-- Dependencies: 203
-- Name: tag_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: rups; Owner: postgres
--

ALTER SEQUENCE rups.tag_tag_id_seq OWNED BY rups.tag.tag_id;


--
-- TOC entry 210 (class 1259 OID 16423)
-- Name: task; Type: TABLE; Schema: rups; Owner: postgres
--

CREATE TABLE rups.task (
    task_id integer NOT NULL,
    name character varying NOT NULL,
    "desc" character varying NOT NULL,
    due_date date,
    tag_id integer,
    status_id integer
);


ALTER TABLE rups.task OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16421)
-- Name: task_task_id_seq; Type: SEQUENCE; Schema: rups; Owner: postgres
--

CREATE SEQUENCE rups.task_task_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rups.task_task_id_seq OWNER TO postgres;

--
-- TOC entry 3058 (class 0 OID 0)
-- Dependencies: 209
-- Name: task_task_id_seq; Type: SEQUENCE OWNED BY; Schema: rups; Owner: postgres
--

ALTER SEQUENCE rups.task_task_id_seq OWNED BY rups.task.task_id;


--
-- TOC entry 218 (class 1259 OID 16494)
-- Name: task_user; Type: TABLE; Schema: rups; Owner: postgres
--

CREATE TABLE rups.task_user (
    task_user_id integer NOT NULL,
    task_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE rups.task_user OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16492)
-- Name: task_user_task_user_id_seq; Type: SEQUENCE; Schema: rups; Owner: postgres
--

CREATE SEQUENCE rups.task_user_task_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rups.task_user_task_user_id_seq OWNER TO postgres;

--
-- TOC entry 3059 (class 0 OID 0)
-- Dependencies: 217
-- Name: task_user_task_user_id_seq; Type: SEQUENCE OWNED BY; Schema: rups; Owner: postgres
--

ALTER SEQUENCE rups.task_user_task_user_id_seq OWNED BY rups.task_user.task_user_id;


--
-- TOC entry 208 (class 1259 OID 16412)
-- Name: user; Type: TABLE; Schema: rups; Owner: postgres
--

CREATE TABLE rups."user" (
    user_id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE rups."user" OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16410)
-- Name: user_user_id_seq; Type: SEQUENCE; Schema: rups; Owner: postgres
--

CREATE SEQUENCE rups.user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rups.user_user_id_seq OWNER TO postgres;

--
-- TOC entry 3060 (class 0 OID 0)
-- Dependencies: 207
-- Name: user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: rups; Owner: postgres
--

ALTER SEQUENCE rups.user_user_id_seq OWNED BY rups."user".user_id;


--
-- TOC entry 2861 (class 2604 OID 16437)
-- Name: panel panel_id; Type: DEFAULT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.panel ALTER COLUMN panel_id SET DEFAULT nextval('rups.panel_panel_id_seq'::regclass);


--
-- TOC entry 2866 (class 2604 OID 16533)
-- Name: panel_task panel_task_id; Type: DEFAULT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.panel_task ALTER COLUMN panel_task_id SET DEFAULT nextval('rups.panel_task_panel_task_id_seq'::regclass);


--
-- TOC entry 2862 (class 2604 OID 16463)
-- Name: project project_id; Type: DEFAULT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.project ALTER COLUMN project_id SET DEFAULT nextval('rups.project_project_id_seq'::regclass);


--
-- TOC entry 2865 (class 2604 OID 16515)
-- Name: project_panel project_panel_id; Type: DEFAULT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.project_panel ALTER COLUMN project_panel_id SET DEFAULT nextval('rups.project_panel_project_panel_id_seq'::regclass);


--
-- TOC entry 2863 (class 2604 OID 16479)
-- Name: project_users project_users_id; Type: DEFAULT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.project_users ALTER COLUMN project_users_id SET DEFAULT nextval('rups.project_users_project_users_id_seq'::regclass);


--
-- TOC entry 2858 (class 2604 OID 16404)
-- Name: status status_id; Type: DEFAULT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.status ALTER COLUMN status_id SET DEFAULT nextval('rups.status_status_id_seq'::regclass);


--
-- TOC entry 2857 (class 2604 OID 16393)
-- Name: tag tag_id; Type: DEFAULT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.tag ALTER COLUMN tag_id SET DEFAULT nextval('rups.tag_tag_id_seq'::regclass);


--
-- TOC entry 2860 (class 2604 OID 16426)
-- Name: task task_id; Type: DEFAULT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.task ALTER COLUMN task_id SET DEFAULT nextval('rups.task_task_id_seq'::regclass);


--
-- TOC entry 2864 (class 2604 OID 16497)
-- Name: task_user task_user_id; Type: DEFAULT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.task_user ALTER COLUMN task_user_id SET DEFAULT nextval('rups.task_user_task_user_id_seq'::regclass);


--
-- TOC entry 2859 (class 2604 OID 16415)
-- Name: user user_id; Type: DEFAULT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups."user" ALTER COLUMN user_id SET DEFAULT nextval('rups.user_user_id_seq'::regclass);


--
-- TOC entry 3034 (class 0 OID 16434)
-- Dependencies: 212
-- Data for Name: panel; Type: TABLE DATA; Schema: rups; Owner: postgres
--

COPY rups.panel (panel_id, name, status_id) FROM stdin;
\.


--
-- TOC entry 3044 (class 0 OID 16530)
-- Dependencies: 222
-- Data for Name: panel_task; Type: TABLE DATA; Schema: rups; Owner: postgres
--

COPY rups.panel_task (panel_task_id, panel_id, task_id) FROM stdin;
\.


--
-- TOC entry 3036 (class 0 OID 16460)
-- Dependencies: 214
-- Data for Name: project; Type: TABLE DATA; Schema: rups; Owner: postgres
--

COPY rups.project (project_id, name, start_date, due_date, status_id) FROM stdin;
\.


--
-- TOC entry 3042 (class 0 OID 16512)
-- Dependencies: 220
-- Data for Name: project_panel; Type: TABLE DATA; Schema: rups; Owner: postgres
--

COPY rups.project_panel (project_panel_id, project_id, panel_id) FROM stdin;
\.


--
-- TOC entry 3038 (class 0 OID 16476)
-- Dependencies: 216
-- Data for Name: project_users; Type: TABLE DATA; Schema: rups; Owner: postgres
--

COPY rups.project_users (project_users_id, project_id, user_id) FROM stdin;
\.


--
-- TOC entry 3028 (class 0 OID 16401)
-- Dependencies: 206
-- Data for Name: status; Type: TABLE DATA; Schema: rups; Owner: postgres
--

COPY rups.status (status_id, value) FROM stdin;
1	Not started
2	Started
3	Done
\.


--
-- TOC entry 3026 (class 0 OID 16390)
-- Dependencies: 204
-- Data for Name: tag; Type: TABLE DATA; Schema: rups; Owner: postgres
--

COPY rups.tag (tag_id, value) FROM stdin;
1	Not important
2	Neutral
3	Important
4	Very important
\.


--
-- TOC entry 3032 (class 0 OID 16423)
-- Dependencies: 210
-- Data for Name: task; Type: TABLE DATA; Schema: rups; Owner: postgres
--

COPY rups.task (task_id, name, "desc", due_date, tag_id, status_id) FROM stdin;
\.


--
-- TOC entry 3040 (class 0 OID 16494)
-- Dependencies: 218
-- Data for Name: task_user; Type: TABLE DATA; Schema: rups; Owner: postgres
--

COPY rups.task_user (task_user_id, task_id, user_id) FROM stdin;
\.


--
-- TOC entry 3030 (class 0 OID 16412)
-- Dependencies: 208
-- Data for Name: user; Type: TABLE DATA; Schema: rups; Owner: postgres
--

COPY rups."user" (user_id, name) FROM stdin;
\.


--
-- TOC entry 3061 (class 0 OID 0)
-- Dependencies: 211
-- Name: panel_panel_id_seq; Type: SEQUENCE SET; Schema: rups; Owner: postgres
--

SELECT pg_catalog.setval('rups.panel_panel_id_seq', 1, false);


--
-- TOC entry 3062 (class 0 OID 0)
-- Dependencies: 221
-- Name: panel_task_panel_task_id_seq; Type: SEQUENCE SET; Schema: rups; Owner: postgres
--

SELECT pg_catalog.setval('rups.panel_task_panel_task_id_seq', 1, false);


--
-- TOC entry 3063 (class 0 OID 0)
-- Dependencies: 219
-- Name: project_panel_project_panel_id_seq; Type: SEQUENCE SET; Schema: rups; Owner: postgres
--

SELECT pg_catalog.setval('rups.project_panel_project_panel_id_seq', 1, false);


--
-- TOC entry 3064 (class 0 OID 0)
-- Dependencies: 213
-- Name: project_project_id_seq; Type: SEQUENCE SET; Schema: rups; Owner: postgres
--

SELECT pg_catalog.setval('rups.project_project_id_seq', 1, false);


--
-- TOC entry 3065 (class 0 OID 0)
-- Dependencies: 215
-- Name: project_users_project_users_id_seq; Type: SEQUENCE SET; Schema: rups; Owner: postgres
--

SELECT pg_catalog.setval('rups.project_users_project_users_id_seq', 1, false);


--
-- TOC entry 3066 (class 0 OID 0)
-- Dependencies: 205
-- Name: status_status_id_seq; Type: SEQUENCE SET; Schema: rups; Owner: postgres
--

SELECT pg_catalog.setval('rups.status_status_id_seq', 3, true);


--
-- TOC entry 3067 (class 0 OID 0)
-- Dependencies: 203
-- Name: tag_tag_id_seq; Type: SEQUENCE SET; Schema: rups; Owner: postgres
--

SELECT pg_catalog.setval('rups.tag_tag_id_seq', 4, true);


--
-- TOC entry 3068 (class 0 OID 0)
-- Dependencies: 209
-- Name: task_task_id_seq; Type: SEQUENCE SET; Schema: rups; Owner: postgres
--

SELECT pg_catalog.setval('rups.task_task_id_seq', 1, false);


--
-- TOC entry 3069 (class 0 OID 0)
-- Dependencies: 217
-- Name: task_user_task_user_id_seq; Type: SEQUENCE SET; Schema: rups; Owner: postgres
--

SELECT pg_catalog.setval('rups.task_user_task_user_id_seq', 1, false);


--
-- TOC entry 3070 (class 0 OID 0)
-- Dependencies: 207
-- Name: user_user_id_seq; Type: SEQUENCE SET; Schema: rups; Owner: postgres
--

SELECT pg_catalog.setval('rups.user_user_id_seq', 1, false);


--
-- TOC entry 2876 (class 2606 OID 16442)
-- Name: panel panel_pk; Type: CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.panel
    ADD CONSTRAINT panel_pk PRIMARY KEY (panel_id);


--
-- TOC entry 2886 (class 2606 OID 16535)
-- Name: panel_task panel_task_pk; Type: CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.panel_task
    ADD CONSTRAINT panel_task_pk PRIMARY KEY (panel_task_id);


--
-- TOC entry 2884 (class 2606 OID 16517)
-- Name: project_panel project_panel_pk; Type: CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.project_panel
    ADD CONSTRAINT project_panel_pk PRIMARY KEY (project_panel_id);


--
-- TOC entry 2878 (class 2606 OID 16468)
-- Name: project project_pk; Type: CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.project
    ADD CONSTRAINT project_pk PRIMARY KEY (project_id);


--
-- TOC entry 2880 (class 2606 OID 16481)
-- Name: project_users project_users_pk; Type: CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.project_users
    ADD CONSTRAINT project_users_pk PRIMARY KEY (project_users_id);


--
-- TOC entry 2870 (class 2606 OID 16409)
-- Name: status status_pk; Type: CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.status
    ADD CONSTRAINT status_pk PRIMARY KEY (status_id);


--
-- TOC entry 2868 (class 2606 OID 16398)
-- Name: tag tag_pk; Type: CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.tag
    ADD CONSTRAINT tag_pk PRIMARY KEY (tag_id);


--
-- TOC entry 2874 (class 2606 OID 16431)
-- Name: task task_pk; Type: CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.task
    ADD CONSTRAINT task_pk PRIMARY KEY (task_id);


--
-- TOC entry 2882 (class 2606 OID 16499)
-- Name: task_user task_user_pk; Type: CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.task_user
    ADD CONSTRAINT task_user_pk PRIMARY KEY (task_user_id);


--
-- TOC entry 2872 (class 2606 OID 16420)
-- Name: user user_pk; Type: CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (user_id);


--
-- TOC entry 2889 (class 2606 OID 16443)
-- Name: panel panel_fk; Type: FK CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.panel
    ADD CONSTRAINT panel_fk FOREIGN KEY (status_id) REFERENCES rups.status(status_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2897 (class 2606 OID 16536)
-- Name: panel_task panel_task_fk; Type: FK CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.panel_task
    ADD CONSTRAINT panel_task_fk FOREIGN KEY (panel_id) REFERENCES rups.panel(panel_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2898 (class 2606 OID 16541)
-- Name: panel_task panel_task_fk_1; Type: FK CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.panel_task
    ADD CONSTRAINT panel_task_fk_1 FOREIGN KEY (task_id) REFERENCES rups.task(task_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2890 (class 2606 OID 16469)
-- Name: project project_fk; Type: FK CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.project
    ADD CONSTRAINT project_fk FOREIGN KEY (status_id) REFERENCES rups.status(status_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2895 (class 2606 OID 16518)
-- Name: project_panel project_panel_fk; Type: FK CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.project_panel
    ADD CONSTRAINT project_panel_fk FOREIGN KEY (project_id) REFERENCES rups.project(project_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2896 (class 2606 OID 16523)
-- Name: project_panel project_panel_fk_1; Type: FK CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.project_panel
    ADD CONSTRAINT project_panel_fk_1 FOREIGN KEY (panel_id) REFERENCES rups.panel(panel_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2891 (class 2606 OID 16482)
-- Name: project_users project_users_fk; Type: FK CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.project_users
    ADD CONSTRAINT project_users_fk FOREIGN KEY (project_id) REFERENCES rups.project(project_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2892 (class 2606 OID 16487)
-- Name: project_users project_users_fk_1; Type: FK CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.project_users
    ADD CONSTRAINT project_users_fk_1 FOREIGN KEY (user_id) REFERENCES rups."user"(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2887 (class 2606 OID 16448)
-- Name: task task_fk; Type: FK CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.task
    ADD CONSTRAINT task_fk FOREIGN KEY (tag_id) REFERENCES rups.tag(tag_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2888 (class 2606 OID 16453)
-- Name: task task_fk_1; Type: FK CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.task
    ADD CONSTRAINT task_fk_1 FOREIGN KEY (status_id) REFERENCES rups.status(status_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2893 (class 2606 OID 16500)
-- Name: task_user task_user_fk; Type: FK CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.task_user
    ADD CONSTRAINT task_user_fk FOREIGN KEY (task_id) REFERENCES rups.task(task_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2894 (class 2606 OID 16505)
-- Name: task_user task_user_fk_1; Type: FK CONSTRAINT; Schema: rups; Owner: postgres
--

ALTER TABLE ONLY rups.task_user
    ADD CONSTRAINT task_user_fk_1 FOREIGN KEY (user_id) REFERENCES rups."user"(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3050 (class 0 OID 0)
-- Dependencies: 7
-- Name: SCHEMA rups; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA rups TO rups;


-- Completed on 2021-11-02 19:50:46

--
-- PostgreSQL database dump complete
--

